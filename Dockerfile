FROM python:3.9-slim
ENV PYTHONUNBUFFERED=1 \
  PORT=8080 \
  POETRY_VERSION=1.4.2 \
  POETRY_VIRTUALENVS_CREATE=false \
  PIP_DISABLE_PIP_VERSION_CHECK=on
ARG IGNORE_DEV_DEPS

RUN mkdir /code
WORKDIR /code

RUN apt-get update && apt-get install -y gcc libsasl2-dev libldap2-dev libssl-dev libmagickwand-dev
RUN pip install "poetry==$POETRY_VERSION"
COPY poetry.lock pyproject.toml /code/
RUN poetry install --no-interaction --no-ansi $(test "$IGNORE_DEV_DEPS" = "True" && echo "--no-dev")

EXPOSE 8080

COPY . /code/

ARG DEBUG
ARG COLLECT_COMPILE_COMPRESS
RUN if [ "$COLLECT_COMPILE_COMPRESS" = "true" ]; then \
  SECRET_KEY=ci python manage.py compilescss; \
  SECRET_KEY=ci python manage.py collectstatic --noinput; \
  SECRET_KEY=ci python manage.py compress; \
  fi


ARG VERSION
ENV VERSION=$VERSION

CMD ["./bin/start.sh"]
