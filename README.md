# Django Canada New York

# Developing

1. `docker-compose up`
2. `docker-compose run --rm web ./manage.py migrate`

## Dependencies

Inside a docker container, run `poetry add <dependency>`

Install dev dependencies with `--dev`. Dependencies that are necessary to run tests should not be dev dependencies.

### VS Code (Optional)

VS Code can do type checking and type inference. However, it requires setting up a virtual environment.

1. Install Python 3 dependencies. For Ubuntu this is `apt install python3-dev python3-venv`
2. Install [poetry](https://python-poetry.org/docs/#installation)
3. Create Python virtual environment `python3 -m venv env`
4. Activate environment `source env/bin/activate`
5. Install packages `poetry install`

## Environment variables

- SECRET_KEY - Set to a random string, do not share between environments
- DEBUG - Set to false in production
- ENVIRONMENT - Used only for branding in wagtail admin
- ALLOWED_HOSTS - Comma separated list of allowed domains
- CSP\_\* - see [django-csp](https://django-csp.readthedocs.io/en/latest/configuration.html)
- CORS_ORIGIN_ALLOW_ALL - Set to False in production
- CORS_ORIGIN_WHITELIST - Comma separated list of allowed CORS origins
- SENTRY_DSN - Defaults to Canada's GlitchTip CMS project. Note this is public and fine to keep in code.
- GOOGLE_CLIENT_ID - If set, wagtail users may log in with Google Apps Auth
- GOOGLE_SECRET - OAUTH Secret for Google Apps Auth
- CLOUDFLARE_API_TOKEN - Cloudflare API Token for wagtail cache invalidation
- CLOUDFLARE_ZONEID - Cloudflare zone
