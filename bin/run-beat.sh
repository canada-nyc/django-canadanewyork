#!/usr/bin/env bash
set -e

exec celery -A django_canada_new_york beat -l info --pidfile=
