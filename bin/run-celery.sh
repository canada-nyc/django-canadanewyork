#!/usr/bin/env bash
set -e

CONCURRENCY="${CONCURRENCY:-1}"

echo "Start celery with CONCURRENCY: $CONCURRENCY"

exec celery -A django_canada_new_york worker -l info --concurrency=$CONCURRENCY
