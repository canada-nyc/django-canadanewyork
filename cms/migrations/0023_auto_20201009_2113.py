# Generated by Django 3.1.2 on 2020-10-09 21:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0022_auto_20201009_1424'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookpage',
            options={},
        ),
        migrations.AlterField(
            model_name='bookpage',
            name='date_text',
            field=models.CharField(blank=True, help_text='If set, will display *instead of* the precise date.', max_length=500, verbose_name='Imprecise Date'),
        ),
    ]
