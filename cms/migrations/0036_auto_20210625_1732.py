# Generated by Django 3.2.3 on 2021-06-25 17:32

import cms.models.base
from django.db import migrations, models
import django.db.models.deletion
import wagtail.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtaildocs', '0012_uploadeddocument'),
        ('cms', '0035_artistpage_artist_featured_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artistpage',
            name='resume_file',
            field=models.ForeignKey(blank=True, help_text='Takes preference over resume text', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtaildocs.document', verbose_name='CV file'),
        ),
        migrations.AlterField(
            model_name='artistpage',
            name='resume_text',
            field=cms.models.base.BodyStreamField([('date_line_item', wagtail.blocks.StructBlock([('date', wagtail.blocks.CharBlock(required=False)), ('text', wagtail.blocks.RichTextBlock(required=False))])), ('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, verbose_name='CV text'),
        ),
    ]
