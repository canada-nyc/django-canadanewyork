# Generated by Django 3.2.13 on 2022-06-16 15:17

import cms.models.base
from django.db import migrations, models
import django.db.models.deletion
import wagtail.blocks
import wagtail.embeds.blocks
import wagtail.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtaildocs', '0012_uploadeddocument'),
        ('cms', '0037_auto_20220404_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artistpage',
            name='artist_media',
            field=wagtail.fields.StreamField([('embed', wagtail.embeds.blocks.EmbedBlock(help_text='Set the full URL of desired embed such as YouTube. Example: https://www.youtube.com/watch?v=SJXMTtvCxRo')), ('photo', wagtail.blocks.StructBlock([('image', wagtail.images.blocks.ImageChooserBlock()), ('extra_info', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(required=False)), ('artist', wagtail.blocks.CharBlock(help_text='Only use if not solo artist in show', required=False)), ('date', wagtail.blocks.CharBlock(required=False)), ('height', wagtail.blocks.DecimalBlock(help_text='Enter height in inches', required=False)), ('width', wagtail.blocks.DecimalBlock(help_text='Enter width in inches', required=False)), ('depth', wagtail.blocks.DecimalBlock(help_text='Enter depth in inches', required=False)), ('dimensions_text', wagtail.blocks.CharBlock(help_text='Only use if the seperate dimension fields do not apply to this piece.', required=False, verbose_name='Dimensions')), ('medium', wagtail.blocks.CharBlock(required=False)), ('extra_text', wagtail.blocks.RichTextBlock(required=False))]))])), ('gfycat_image', wagtail.blocks.StructBlock([('gfycat_id', wagtail.blocks.CharBlock(help_text="The part after 'https://gfycat.com/detail/'.         For example, the ID for         'https://gfycat.com/detail/WarmSmartGeese?tagname=civil%20war&tvmode=0'         would be 'WarmSmartGeese'")), ('extra_info', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(required=False)), ('artist', wagtail.blocks.CharBlock(help_text='Only use if not solo artist in show', required=False)), ('date', wagtail.blocks.CharBlock(required=False)), ('height', wagtail.blocks.DecimalBlock(help_text='Enter height in inches', required=False)), ('width', wagtail.blocks.DecimalBlock(help_text='Enter width in inches', required=False)), ('depth', wagtail.blocks.DecimalBlock(help_text='Enter depth in inches', required=False)), ('dimensions_text', wagtail.blocks.CharBlock(help_text='Only use if the seperate dimension fields do not apply to this piece.', required=False, verbose_name='Dimensions')), ('medium', wagtail.blocks.CharBlock(required=False)), ('extra_text', wagtail.blocks.RichTextBlock(required=False))]))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='artistpage',
            name='resume_file',
            field=models.ForeignKey(blank=True, help_text='Takes preference over CV text', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtaildocs.document', verbose_name='CV file'),
        ),
        migrations.AlterField(
            model_name='artistpage',
            name='resume_text',
            field=cms.models.base.BodyStreamField([('date_line_item', wagtail.blocks.StructBlock([('date', wagtail.blocks.CharBlock(required=False)), ('text', wagtail.blocks.RichTextBlock(required=False))])), ('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True, verbose_name='CV text'),
        ),
        migrations.AlterField(
            model_name='bookpage',
            name='description',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='contactpage',
            name='body',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='currentexhibition',
            name='extra_info',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='exhibitionpage',
            name='exhibition_media',
            field=wagtail.fields.StreamField([('embed', wagtail.embeds.blocks.EmbedBlock(help_text='Set the full URL of desired embed such as YouTube. Example: https://www.youtube.com/watch?v=SJXMTtvCxRo')), ('photo', wagtail.blocks.StructBlock([('image', wagtail.images.blocks.ImageChooserBlock()), ('extra_info', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(required=False)), ('artist', wagtail.blocks.CharBlock(help_text='Only use if not solo artist in show', required=False)), ('date', wagtail.blocks.CharBlock(required=False)), ('height', wagtail.blocks.DecimalBlock(help_text='Enter height in inches', required=False)), ('width', wagtail.blocks.DecimalBlock(help_text='Enter width in inches', required=False)), ('depth', wagtail.blocks.DecimalBlock(help_text='Enter depth in inches', required=False)), ('dimensions_text', wagtail.blocks.CharBlock(help_text='Only use if the seperate dimension fields do not apply to this piece.', required=False, verbose_name='Dimensions')), ('medium', wagtail.blocks.CharBlock(required=False)), ('extra_text', wagtail.blocks.RichTextBlock(required=False))]))])), ('gfycat_image', wagtail.blocks.StructBlock([('gfycat_id', wagtail.blocks.CharBlock(help_text="The part after 'https://gfycat.com/detail/'.         For example, the ID for         'https://gfycat.com/detail/WarmSmartGeese?tagname=civil%20war&tvmode=0'         would be 'WarmSmartGeese'")), ('extra_info', wagtail.blocks.StructBlock([('title', wagtail.blocks.CharBlock(required=False)), ('artist', wagtail.blocks.CharBlock(help_text='Only use if not solo artist in show', required=False)), ('date', wagtail.blocks.CharBlock(required=False)), ('height', wagtail.blocks.DecimalBlock(help_text='Enter height in inches', required=False)), ('width', wagtail.blocks.DecimalBlock(help_text='Enter width in inches', required=False)), ('depth', wagtail.blocks.DecimalBlock(help_text='Enter depth in inches', required=False)), ('dimensions_text', wagtail.blocks.CharBlock(help_text='Only use if the seperate dimension fields do not apply to this piece.', required=False, verbose_name='Dimensions')), ('medium', wagtail.blocks.CharBlock(required=False)), ('extra_text', wagtail.blocks.RichTextBlock(required=False))]))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='exhibitionpage',
            name='press_release',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='press',
            name='content',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
        migrations.AlterField(
            model_name='update',
            name='description',
            field=cms.models.base.BodyStreamField([('rich_text', wagtail.blocks.RichTextBlock()), ('spacer', wagtail.blocks.StructBlock([('size', wagtail.blocks.ChoiceBlock(choices=[(20, 'Small (20px)'), (40, 'Medium (40px)'), (60, 'Large (60px)')], required=False)), ('custom_height', wagtail.blocks.IntegerBlock(help_text="Entering a number here will set the height in pixels and overrule the 'size' choice.", min_value=0, required=False)), ('anchor', wagtail.blocks.CharBlock(help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page.", required=False))]))], blank=True, use_json_field=True),
        ),
    ]
