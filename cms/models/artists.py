import logging
from django.db import models
from django.apps import apps
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail import blocks
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.embeds.blocks import EmbedBlock
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
)
from .base import BodyStreamField
from .photos import PhotoBlock, YouTubeArtBlock, VimeoArtBlock, GfyCatArtBlock
from .shared import get_responsive_image, get_thumbnail_image
from .base import BaseIndexPage

logger = logging.getLogger(__name__)


class DateLineItem(blocks.StructBlock):
    date = blocks.CharBlock(required=False)
    text = blocks.RichTextBlock(required=False)


class ArtistIndexPage(BaseIndexPage):
    parent_page_types = ["cms.homepage"]
    subpage_types = ["cms.ArtistPage"]

    template = 'cms/list_grid.html'

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_resources = (
            ArtistPage.objects.child_of(self)
            .live()
            .filter(visible=True)
            .order_by("last_name")
        )
        resources = self.get_pagination(request, all_resources)
        context["items"] = resources
        return context


class ArtistPage(RoutablePageMixin, Page):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    website = models.URLField(blank=True)
    resume_text = BodyStreamField([
        ('date_line_item', DateLineItem()),
    ], blank=True,
        use_json_field=True,
        verbose_name="CV text")
    resume_file = models.ForeignKey(
        "wagtaildocs.Document",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name="CV file",
        help_text="Takes preference over CV text",
    )
    artist_featured_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text=(
            "Will be used on the Artists index page if used; otherwise, that "
            "page will use the first Artist Media image"
        ),
    )
    artist_media = StreamField(
        [
            (
                "embed",
                EmbedBlock(
                    help_text="Set the full URL of desired embed such as YouTube. Example: https://www.youtube.com/watch?v=SJXMTtvCxRo"
                ),
            ),
            ("photo", PhotoBlock()),
            ("gfycat_image", GfyCatArtBlock()),
        ],
        blank=True,
        use_json_field=True
    )
    visible = models.BooleanField(
        default=False, help_text="Whether it appears in artists list and has a page"
    )

    def __str__(self):
        return ' '.join([self.first_name, self.last_name])

    def list_item(self):
        return self

    @property
    def index_page_image(self):
        if self.artist_featured_image:
            return self.artist_featured_image
        else:
            for block in self.artist_media:
                if block.block_type == "photo":
                    return block.value["image"]

    def grid_thumbnail(self):
        try:
            return get_thumbnail_image(
                self.index_page_image,
                getattr(self.get_parent().specific, 'column_count', None)
            )
        except IOError:
            logger.warning("Unable to fetch image for %s" %
                           self.index_page_image)

    def first_image(self):
        for block in self.artist_media:
            if block.block_type == "photo":
                try:
                    return get_responsive_image(self.index_page_image)
                except IOError:
                    logger.warning("Unable to fetch image for %s" %
                                   self.index_page_image)

    @property
    def all_press(self):
        return apps.get_model("cms", "Press").objects.filter(
            models.Q(artist=self) | models.Q(exhibition__artists__in=[self])
        )

    @property
    def all_exhibitions(self):
        return apps.get_model("cms", "ExhibitionPage").objects.live().filter(
            models.Q(artists__in=[self])
        ).order_by("-start_date")

    @property
    def all_books(self):
        return apps.get_model("cms", "BookPage").objects.filter(
            models.Q(artist__in=[self])
        )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("first_name"),
                FieldPanel("last_name"),
                FieldPanel("visible"),
                FieldPanel("website"),
            ],
            heading="Name",
        ),
        FieldPanel("artist_featured_image"),
        MultiFieldPanel(
            [FieldPanel("resume_file"),
             FieldPanel("resume_text"), ],
            heading="CV",
        ),
        FieldPanel("artist_media"),
    ]

    parent_page_types = ["cms.ArtistIndexPage"]
    subpage_types = []

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        if hasattr(self, "is_image_page"):
            responsive_images = []
            for block in self.artist_media:
                if block.block_type == "photo":
                    try:
                        responsive_image = get_responsive_image(
                            block.value["image"])
                        if block.value["extra_info"]:
                            try:
                                responsive_image["caption"] = block.value["extra_info"].caption
                            except:
                                logger.warning(
                                    "'NoneType' object does not support item assignment")
                        responsive_images.append(responsive_image)
                    except IOError:
                        logger.warning("Unable to fetch image for %s" %
                                       self.index_page_image)
                else:
                    responsive_images.append(block)
            context["image_gallery"] = responsive_images
        return context

    @route(r"^images/$")
    def artist_images(self, request, *args, **kwargs):
        self.template = "cms/artist_page_images.html"
        self.is_image_page = True
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^resume/$")
    def artist_resume(self, request, *args, **kwargs):
        self.template = "cms/artist_page_resume.html"
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^press/$")
    def artist_press(self, request, *args, **kwargs):
        self.template = "cms/artist_page_press.html"
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^exhibitions/$")
    def artist_exhibitions(self, request, *args, **kwargs):
        self.template = "cms/artist_page_exhibitions.html"
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^books/$")
    def artist_books(self, request, *args, **kwargs):
        self.template = "cms/artist_page_books.html"
        return Page.serve(self, request, *args, **kwargs)
