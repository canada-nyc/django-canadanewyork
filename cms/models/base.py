from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from wagtail.api import APIField
from wagtail.blocks import RichTextBlock
from wagtail.fields import StreamField
from wagtail.models import Page, Orderable
from wagtail.admin.panels import FieldPanel
from ..shared_blocks import spacer_tuple


class BodyStreamField(StreamField):
    body_blocks = [
        ("rich_text", RichTextBlock()),
        spacer_tuple,
    ]

    use_json_field=True

    def __init__(self, block_types, blank=True, **kwargs):
        block_types += self.body_blocks
        super().__init__(block_types, blank=blank, **kwargs)


LIST_VIEW = ("list", "List of links")
ROLLOVER_LIST_VIEW = ("rollover-list", "List of links + rollover")
IMAGE_LIST_VIEW = ("image-list", "List of link + image")

INDEX_PAGE_VIEWS = [
    LIST_VIEW,
    ROLLOVER_LIST_VIEW,
    IMAGE_LIST_VIEW,
    ("grid-two", "Grid, two per row"),
    ("grid-three", "Grid, three per row"),
    ("grid-four", "Grid, four per row"),
]

class BaseIndexPage(Page):
    layout = models.CharField(
        max_length=50,
        choices=INDEX_PAGE_VIEWS,
        blank=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("layout"),
    ]

    @property
    def column_count(self):
        layout = self.layout
        if layout == "image-list":
            return 1.333
        if (
            layout == "grid-two" or
            layout == "rollover-list"
        ):
            return 2
        if layout == "grid-three":
            return 3
        if layout == "grid-four":
            return 4
        return 1

    @property
    def list_or_grid(self):
        if (
            self.layout == LIST_VIEW[0] or
            self.layout == ROLLOVER_LIST_VIEW[0] or
            self.layout == IMAGE_LIST_VIEW[0]
        ):
            return "list"
        else:
            return "grid"
    
    def get_pagination(self, request, all_resources, num_pages=100):
        paginator = Paginator(all_resources, num_pages)
        page = request.GET.get("page")
        try:
            resources = paginator.page(page)
        except PageNotAnInteger:
            resources = paginator.page(1)
        except EmptyPage:
            resources = paginator.page(paginator.num_pages)
        return resources

    class Meta:
        abstract = True


class BasePage(Page):
    transparent_header = models.BooleanField(default=False)

    def ancestors(self):
        site = self.get_site()
        return [
            {
                "id": page.id,
                "title": page.title,
                "slug": page.get_url(current_site=site),
            }
            for page in self.get_ancestors(inclusive=True).in_site(site)
        ]

    api_fields = [
        APIField("ancestors"),
        APIField("transparent_header"),
    ]

    content_panels = [FieldPanel("transparent_header"),] + Page.content_panels

    class Meta:
        abstract = True


class PlaceholderPage(BasePage):
    pass


class MediaImageBase(Orderable):
    image = models.ForeignKey("wagtailimages.Image", on_delete=models.CASCADE)
    caption = models.CharField(max_length=500, blank=True)

    panels = [
        FieldPanel("image"),
        FieldPanel("caption"),
    ]

    class Meta:
        abstract = True
