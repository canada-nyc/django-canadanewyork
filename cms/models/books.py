import logging
from django.db import models
from django.template.loader import render_to_string
from wagtail.admin.panels import (
    FieldPanel,
    FieldRowPanel,
    MultiFieldPanel,
    InlinePanel,
)
from wagtail.models import Page
from wagtail.embeds.blocks import EmbedBlock
from modelcluster.fields import ParentalKey
from .base import BodyStreamField, MediaImageBase
from .photos import PhotoBlock, GfyCatArtBlock
from .shared import get_responsive_image, get_thumbnail_image
from .base import BaseIndexPage

logger = logging.getLogger(__name__)


class BookIndexPage(BaseIndexPage):
    parent_page_types = ["cms.homepage"]
    subpage_types = ["cms.BookPage"]

    template = 'cms/list_grid.html'

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["items"] = (
            BookPage.objects.child_of(self)
            .live()
            .order_by("artist__last_name", "artist__first_name", "-date")
            .prefetch_related("book_images__image")
            .select_related("artist")
        )
        return context


class BookImage(MediaImageBase):
    book_page = ParentalKey(
        "cms.BookPage", on_delete=models.CASCADE, related_name="book_images"
    )


class BookPage(Page):
    artist = models.ForeignKey(
        "cms.ArtistPage", related_name="books", null=True, on_delete=models.SET_NULL
    )
    description = BodyStreamField([], blank=True, use_json_field=True)
    price = models.PositiveSmallIntegerField(
        blank=True, null=True, help_text="If blank, will not show buy button."
    )
    out_of_stock = models.BooleanField(
        default=False,
        help_text="If true, won't allow people to purchase and will display text saying out of stock",
    )
    date = models.DateField(verbose_name="Precise Date",
                            help_text="Used for ordering")
    date_text = models.CharField(
        verbose_name="Imprecise Date",
        max_length=500,
        blank=True,
        help_text="If set, will display *instead of* the precise date.",
    )

    @property
    def parent_column_count(self):
        parent_page = self.get_parent()
        if parent_page.specific_class == BookIndexPage:
            return parent_page.specific.column_count
        else:
            return None

    @property
    def index_page_image(self):
        book_image = self.book_images.all()
        if book_image:
            return book_image[0].image

    def grid_thumbnail(self):
        try:
            return get_thumbnail_image(
                self.index_page_image,
                getattr(self.get_parent().specific, 'column_count', None)
            )
        except IOError:
            logger.warning("Unable to fetch image for %s" %
                           self.index_page_image)

    def first_image(self):
        try:
            return get_responsive_image(self.index_page_image)
        except IOError:
            logger.warning("Unable to fetch image for %s" %
                           self.index_page_image)

    def list_item(self):
        list_item_template = 'cms/book_list_item.html'
        return render_to_string(list_item_template, {
            'object': self,
        })

    @property
    def can_buy(self):
        return self.price and not self.out_of_stock

    parent_page_types = ["cms.BookIndexPage"]
    content_panels = Page.content_panels + [
        InlinePanel("book_images", heading="Images"),
        MultiFieldPanel(
            [
                FieldPanel("artist"),
                FieldRowPanel(
                    children=[FieldPanel("price"),
                              FieldPanel("out_of_stock"), ]
                ),
                FieldRowPanel(children=[FieldPanel(
                    "date"), FieldPanel("date_text"), ]),
                FieldPanel("description"),
            ],
            heading="Book Information",
        ),
    ]
