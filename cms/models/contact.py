from wagtail.models import Page
from wagtail.admin.panels import FieldPanel
from .base import BodyStreamField


class ContactPage(Page):
    body = BodyStreamField([], blank=True, use_json_field=True)

    parent_page_types = ["cms.HomePage"]

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]
