from django import forms
from django.apps import apps
from django.db import models
from django.http.response import Http404
from django.template.loader import render_to_string
from libs.common.utils import sentence_join
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail import blocks
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.embeds.blocks import EmbedBlock
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
    FieldRowPanel,
)
from wagtailautocomplete.edit_handlers import AutocompletePanel
from .base import BodyStreamField, BaseIndexPage
from .photos import PhotoBlock, YouTubeArtBlock, VimeoArtBlock, GfyCatArtBlock
from .shared import get_responsive_image, get_thumbnail_image
from .artists import ArtistPage


LIST_VIEW = ("list", "List of links")
ROLLOVER_LIST_VIEW = ("rollover-list", "List of links + rollover")

INDEX_PAGE_VIEWS = [
    LIST_VIEW,
    ROLLOVER_LIST_VIEW,
    ("grid-two", "Grid, two per row"),
    ("grid-three", "Grid, three per row"),
    ("grid-four", "Grid, four per row"),
]


class ExhibitionIndexPage(RoutablePageMixin, BaseIndexPage):
    parent_page_types = ["cms.homepage"]
    subpage_types = ["cms.ExhibitionPage"]

    template = "cms/list_grid.html"

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_resources = ExhibitionPage.objects.child_of(self).order_by("-start_date")
        resources = self.get_pagination(request, all_resources)
        context["items"] = resources
        return context

    def get_exhibition_page(self, year, slug):
        try:
            page = (
                ExhibitionPage.objects.live()
                .child_of(self)
                .get(start_date__year=year, slug=slug)
            )
        except ExhibitionPage.DoesNotExist:
            raise Http404
        return page

    @route(r"^(?P<year>[0-9]{4})/(?P<slug>[\w-]+)/?$")
    def by_date(self, request, year, slug, name="exhibition-by-date"):
        """Serve a single page at URL (eg. .../2018/my-title/)"""
        page = self.get_exhibition_page(year, slug)
        return page.serve(request)

    @route(r"^(?P<year>[0-9]{4})/(?P<slug>[\w-]+)/images/$")
    def exhibition_images(self, request, year, slug, *args, **kwargs):
        page = self.get_exhibition_page(year, slug)
        page.template = "cms/artist_page_images.html"
        page.is_image_page = True
        return page.serve(request, *args, **kwargs)

    @route(r"^(?P<year>[0-9]{4})/(?P<slug>[\w-]+)/press/$")
    def exhibition_press(self, request, year, slug, *args, **kwargs):
        page = self.get_exhibition_page(year, slug)
        page.template = "cms/artist_page_press.html"
        return page.serve(request, *args, **kwargs)

    @route(r"^(?P<year>[0-9]{4})/(?P<slug>[\w-]+)/press-release/$")
    def exhibition_press_release(self, request, year, slug, *args, **kwargs):
        page = self.get_exhibition_page(year, slug)
        page.template = "cms/exhibition_page_press_release.html"
        return page.serve(request, *args, **kwargs)


class ExhibitionPage(RoutablePageMixin, Page):
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    by_appointment = models.BooleanField(
        default=False,
        help_text="If checked, dates will be replaced with 'By appointment only' on list views"
    )
    artists = ParentalManyToManyField("cms.ArtistPage", blank=True)
    curated_by = models.CharField(max_length=500, blank=True)
    location = models.ForeignKey(
        "cms.Location", null=True, blank=True, on_delete=models.SET_NULL
    )
    exhibition_media = StreamField(
        [
            (
                "embed",
                EmbedBlock(
                    help_text="Set the full URL of desired embed such as YouTube. Example: https://www.youtube.com/watch?v=SJXMTtvCxRo"
                ),
            ),
            ("photo", PhotoBlock()),
            ("gfycat_image", GfyCatArtBlock()),
        ],
        blank=True,
        use_json_field=True,
    )
    press_release = BodyStreamField([], blank=True, use_json_field=True)
    press_release_photo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text=(
            "Used if it is the current exhibition, on the homepage. If not "
            "specified will use first of the uploaded photos on the homepage"
        ),
    )

    external_link = models.URLField(max_length=500, blank=True)
    external_link_text = models.CharField(
        max_length=500, blank=True, default="Viewing Room"
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    children=[
                        FieldPanel("start_date"),
                        FieldPanel("end_date"),
                        FieldPanel("by_appointment")
                    ]
                ),
                FieldRowPanel(
                    children=[
                        FieldPanel("location"),
                    ]
                ),
            ],
            heading="Date and Location",
        ),
        AutocompletePanel("artists", target_model=ArtistPage),
        FieldPanel("curated_by"),
        MultiFieldPanel(
            [
                FieldPanel("press_release", classname="full"),
                FieldPanel("press_release_photo"),
            ],
            heading="Press Release",
        ),
        MultiFieldPanel(
            [
                FieldRowPanel(
                    children=[
                        FieldPanel("external_link"),
                        FieldPanel("external_link_text"),
                    ]
                )
            ],
            heading="External Link",
        ),
        FieldPanel("exhibition_media"),
    ]

    parent_page_types = ["cms.ExhibitionIndexPage"]
    subpage_types = []

    @property
    def not_group_show(self):
        if 0 < self.artists.count() < 3:
            return True

    @property
    def join_artists(self):
        return sentence_join(list(map(str, self.artists.all())))

    def list_item(self):
        list_item_template = "cms/exhibition_list_item.html"
        return render_to_string(
            list_item_template,
            {
                "object": self,
            },
        )

    @property
    def index_page_image(self):
        if self.press_release_photo:
            return self.press_release_photo
        else:
            for block in self.exhibition_media:
                if block.block_type == "photo":
                    return block.value["image"]

    @property
    def all_press(self):
        return apps.get_model("cms", "Press").objects.filter(models.Q(exhibition=self))

    def grid_thumbnail(self):
        return get_thumbnail_image(
            self.index_page_image,
            getattr(self.get_parent().specific, "column_count", None),
        )

    @property
    def responsive_pr_photo(self):
        if self.press_release_photo:
            return get_responsive_image(self.press_release_photo)

    def first_image(self):
        for block in self.exhibition_media:
            if block.block_type == "photo":
                return get_responsive_image(self.index_page_image)

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        if hasattr(self, "is_image_page"):
            responsive_images = []
            for block in self.exhibition_media:
                if block.block_type == "photo":
                    try:
                        responsive_image = get_responsive_image(block.value["image"])
                        if block.value["extra_info"]:
                            try:
                                responsive_image["caption"] = block.value[
                                    "extra_info"
                                ].caption
                            except:
                                continue
                        responsive_images.append(responsive_image)
                    except IOError:
                        logger.warning(
                            "Unable to fetch image for %s" % self.index_page_image
                        )
                else:
                    responsive_images.append(block)
            context["image_gallery"] = responsive_images
        return context

    def set_url_path(self, parent):
        # initially set the attribute self.url_path using the normal operation
        super().set_url_path(parent=parent)
        self.url_path = self.url_path.replace(
            self.slug, "{:%Y/}".format(self.start_date) + self.slug
        )
