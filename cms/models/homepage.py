from django.db import models
from modelcluster.fields import ParentalKey
from wagtail import blocks
from wagtail.models import Page, Orderable
from wagtail.fields import StreamField
from wagtail.admin.panels import (
    FieldPanel, InlinePanel, PageChooserPanel, 
)
from .exhibitions import ExhibitionPage
from .base import BodyStreamField


class CurrentExhibition(Orderable):
    page = ParentalKey(
        "cms.HomePage", on_delete=models.CASCADE, related_name="current_exhibitions"
    )
    exhibition = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    extra_info = BodyStreamField([], blank=True, use_json_field=True)
    use_external_link = models.BooleanField(
        default=False,
        help_text="If checked, clicking the image will go to the external link instead of the exhibition page"
    )

    panels = [
        PageChooserPanel("exhibition", "cms.ExhibitionPage"),
        FieldPanel("extra_info"),
        FieldPanel("use_external_link"),
    ]


class HomePage(Page):
    body = BodyStreamField([], blank=True, use_json_field=True)

    content_panels = Page.content_panels + [
        InlinePanel("current_exhibitions", label="Current Exhibitions"),
        FieldPanel("body"),
    ]
