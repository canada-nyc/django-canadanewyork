from django.db import models

class Location (models.Model):
    name = models.CharField(max_length=50, blank=True)
    address = models.CharField(max_length=500, blank=True, help_text="For display on lists and pages")

    def __str__(self):
        return self.address
