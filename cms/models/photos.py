from decimal import Decimal, ROUND_DOWN
from fractions import Fraction
from django.template.loader import render_to_string
from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock


class CaptionStructValue(blocks.StructValue):
    INCHES_PER_CM = 2.54

    dimension_field_attributes = {
        # 'blank': True,
        # 'null': True,
        # 'max_digits': 10,
        'decimal_places': 4,
    }

    DECIMAL_PLACES = Decimal(10) ** (-1 * dimension_field_attributes['decimal_places'])

    def remove_exponent(self, d):
        '''
        Remove exponent and trailing zeros.

        >>> remove_exponent(Decimal('5E+3'))
        Decimal('5000')
        '''
        return d.quantize(Decimal(1)) if d == d.to_integral() else d.normalize()

    def round_decimal(self, number):
        return self.remove_exponent(Decimal(number).normalize())

    @property
    def dimensions(self):
        return list(map(self.round_decimal, [_f for _f in [self.get("height"), self.get("width"), self.get("depth")] if _f]))

    def convert_inches_to_cm(self, inches):
        cm = inches * Decimal(self.INCHES_PER_CM)
        return cm.quantize(self.DECIMAL_PLACES)

    @property
    def dimensions_cm(self):
        return list(map(self.round_decimal, list(map(self.convert_inches_to_cm, self.dimensions))))

    def decimal_to_fraction(self, decimal):
        integer = decimal.to_integral_value(ROUND_DOWN)
        decimal_part = decimal - integer
        if not decimal_part:
            return str(integer)
        fraction = Fraction(decimal_part)
        fraction_string = '{}/{}'.format(fraction.numerator, fraction.denominator)
        if integer:
            return '{} {}'.format(integer, fraction_string)
        return fraction_string

    def full_dimensions(self):
        if self.get('dimensions_text'):
            return self.get('dimensions_text')
        if self.dimensions:
            inches_dimensions = list(map(self.decimal_to_fraction, self.dimensions))
            cm_dimensions = list(map(str, self.dimensions_cm))
            return '{} in ({} cm)'.format(' x '.join(inches_dimensions), ' x '.join(cm_dimensions))

    def caption(self):
        caption_template = 'cms/full_caption.html'

        photo = {
            "artist_text": self.get('artist'),
            "caption": self.get('extra_text'),
            "date": self.get('date'),
            "full_dimensions": self.full_dimensions,
            "medium": self.get('medium'),
            "title": self.get('title')
        }
        return render_to_string(caption_template, {'photo': photo})


class PhotoInfoBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=False)
    artist = blocks.CharBlock(
        required=False,
        help_text='Only use if not solo artist in show'
    )
    date = blocks.CharBlock(required=False)
    height = blocks.DecimalBlock(required=False, help_text='Enter height in inches')
    width = blocks.DecimalBlock(required=False, help_text='Enter width in inches')
    depth = blocks.DecimalBlock(required=False, help_text='Enter depth in inches')
    dimensions_text = blocks.CharBlock(
        required=False,
        help_text='Only use if the seperate dimension fields do not apply to this piece.',
        verbose_name='Dimensions',
    )
    medium = blocks.CharBlock(required=False)
    extra_text = blocks.RichTextBlock(required=False)

    class Meta:
        value_class = CaptionStructValue


class PhotoBlock(blocks.StructBlock):
    image = ImageChooserBlock()
    extra_info = PhotoInfoBlock()

    class Meta:
        icon = 'image'


class YouTubeArtBlock(blocks.StructBlock):
    youtube_id = blocks.CharBlock(
        help_text="The part after 'https://www.youtube.com/watch?v='. \
            For example, the ID for \
            'https://www.youtube.com/watch?v=2IWBHAZ9Q_s' \
            would be '2IWBHAZ9Q_s'."
    )
    extra_info = PhotoInfoBlock()

    class Meta:
        icon = 'media'


class VimeoArtBlock(blocks.StructBlock):
    vimeo_id = blocks.CharBlock(
        help_text="The part after 'https://www.vimeo.com/'. \
        For example, the ID for 'https://vimeo.com/45830194' \
        would be '45830194'."
    )
    extra_info = PhotoInfoBlock()

    class Meta:
        icon = 'media'


class GfyCatArtBlock(blocks.StructBlock):
    gfycat_id = blocks.CharBlock(
        help_text="The part after 'https://gfycat.com/detail/'. \
        For example, the ID for \
        'https://gfycat.com/detail/WarmSmartGeese?tagname=civil%20war&tvmode=0' \
        would be 'WarmSmartGeese'"
    )
    extra_info = PhotoInfoBlock()

    class Meta:
        icon = 'media'
