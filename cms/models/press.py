from django.db import models
from django.urls import reverse
from django.core.exceptions import ValidationError
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
    FieldRowPanel,
)
from .artists import ArtistPage
from .exhibitions import ExhibitionPage
from libs.slugify.fields import SlugifyField
from .base import BodyStreamField


SLUG_FIELD_NAMES = ("publisher", "title", "artist", "exhibition")


class Press(models.Model):
    title = models.CharField(max_length=500, blank=True)
    content = BodyStreamField([], blank=True, use_json_field=True)
    content_file = models.ForeignKey(
        "wagtaildocs.Document",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content_link = models.URLField(blank=True)

    date = models.DateField(
        verbose_name='Precise Date',
        help_text='Used for ordering'
    )
    date_text = models.CharField(
        verbose_name='Imprecise Date',
        max_length=500,
        blank=True,
        help_text="If set, will display *instead of* the precise date."
    )

    publisher = models.CharField(max_length=50, blank=True)
    author = models.CharField(max_length=500, blank=True)
    pages_range = models.CharField(max_length=50, blank=True)

    artist = models.ForeignKey(
        ArtistPage,
        blank=True,
        null=True,
        related_name='press',
        on_delete=models.SET_NULL,
    )
    exhibition = models.ForeignKey(
        ExhibitionPage,
        blank=True,
        null=True,
        related_name='press',
        on_delete=models.SET_NULL,
    )

    slug = SlugifyField(
        populate_from=('date_year', 'slug_title',),
        slug_template='{}/{}',
        unique=True
    )

    class Meta:
        ordering = ['-date']
        verbose_name_plural = "press"

    def __str__(self):
        return self.title

    def clean(self):
        if not any(self._slug_field_values):
            raise ValidationError(
                'At least one of the following must be filled in: ' + str(SLUG_FIELD_NAMES))

    @property
    def slug_title(self):
        return '-'.join(map(repr, self._slug_field_values))

    @property
    def date_year(self):
        return self.date.year

    @property
    def _slug_field_values(self):
        values = [getattr(self, field_name) for field_name in SLUG_FIELD_NAMES]
        return [_f for _f in values if _f]

    """
    A press item can link to an external url or to a file; if only one of those
    is available and there is no other content, link directly to save a click
    """

    def get_absolute_url(self):
        url = reverse('press-detail', kwargs={'slug': self.slug})
        has_content = len(self.content) > 0
        has_content_link = self.content_link != ''
        has_content_file = self.content_file is not None

        if has_content or (has_content_link and has_content_file):
            return url

        if has_content_link:
            return self.content_link

        if has_content_file:
            return self.content_file.url

        return url

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("title"),
                FieldRowPanel(children=[
                    FieldPanel("date"),
                    FieldPanel("date_text"),
                ]),
                FieldPanel("publisher"),
                FieldPanel("author"),
                FieldPanel("pages_range"),
            ],
            heading="Basic information"
        ),
        MultiFieldPanel(
            [
                FieldPanel("content"),
                FieldPanel("content_file"),
                FieldPanel("content_link"),
            ],
            heading="Article"
        ),
        MultiFieldPanel(
            [
                FieldPanel("artist"),
                FieldPanel("exhibition"),
            ],
            heading="Related to"
        ),
    ]

    search_fields = ["description"]
