from django.conf import settings


"""
width = height is a square, but this should include things that are like 2px off
"""
def is_square(image):
    if (image.width == image.height):
        return True
    elif (round(image.width / image.height * 100) == 100):
        return True
    return False


"""
Goals:
1. Build a srcset for responsive images
2. If the image is landscape, make it full width. If not, adjust the width
   so portrait-oriented images don't dominate the visible area
3. Wagtail doesn't upscale. We'd rather have a pixelated image that fills
    the space than a high-dpi image that's too small
"""
def get_responsive_image(image):
    if (image):
        width = image.width
        height = image.height

        image_width = settings.SECTION_PIXEL_WIDTH
        if is_square(image):
            image_width = round(settings.SECTION_PIXEL_WIDTH * 0.75)
        elif width < height:
            image_width = round(image_width * (width / height) * 0.8335)
        if image_width < settings.SECTION_PIXEL_WIDTH * 0.5:
            image_width = round(settings.SECTION_PIXEL_WIDTH * 0.5)

        return build_srcset(image, image_width, width)

    return None

"""
Get thumbnail for a grid image
"""
def get_thumbnail_image(image, number_of_columns):
    if (image and number_of_columns):
        width = image.width

        SECTION_WIDTH = settings.SECTION_PIXEL_WIDTH
        image_width = round(SECTION_WIDTH / number_of_columns) - 50

        return build_srcset(image, image_width, width)

    return None


def build_srcset(image, image_width, overall_width):
    img_1x = image.get_rendition(f"width-{str(image_width)}").url
    srcset = f"{img_1x} 1x"

    if (image_width * 2 < overall_width):
        img_2x = image.get_rendition(f"width-{str(image_width * 2)}").url
        srcset = srcset + f", {img_2x} 2x"

    if (image_width * 3 < overall_width):
        img_3x = image.get_rendition(f"width-{str(image_width * 3)}").url
        srcset = srcset + f", {img_3x} 3x"

    return {
        "image_width": image_width,
        "src": img_1x,
        "srcset": srcset,
        "original_src": image.file.url
    }