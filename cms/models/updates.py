import logging
from django.db import models
from wagtail.admin.panels import FieldPanel, InlinePanel
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from .base import BodyStreamField, MediaImageBase
from .shared import get_responsive_image, get_thumbnail_image
from .base import BaseIndexPage

logger = logging.getLogger(__name__)


class UpdateIndexPage(BaseIndexPage):
    parent_page_types = ["cms.homepage"]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_resources = (
            Update.objects.all()
            .order_by("-post_date")
            .prefetch_related("update_images__image")
        )
        resources = self.get_pagination(request, all_resources)
        context["items"] = resources
        return context


class UpdateImage(MediaImageBase):
    update = ParentalKey(
        "cms.Update", on_delete=models.CASCADE, related_name="update_images"
    )


class Update(ClusterableModel):
    description = BodyStreamField([], blank=True, use_json_field=True)
    post_date = models.DateField(verbose_name="Date")
    external_link = models.URLField(
        blank=True, help_text="Setting this will cause the update to link to this URL"
    )

    def grid_thumbnail(self, column_count):
        update_image = self.update_images.all()
        if update_image:
            try:
                return get_thumbnail_image(update_image[0].image, column_count)
            except IOError:
                logger.warning("Unable to fetch image for %s" % update_image)

    @property
    def grid_thumbnail_two(self):
        return self.grid_thumbnail(2)

    @property
    def grid_thumbnail_three(self):
        return self.grid_thumbnail(3)

    @property
    def grid_thumbnail_four(self):
        return self.grid_thumbnail(4)

    class Meta:
        ordering = ["-post_date"]

    def __str__(self):
        return str(self.post_date.isoformat())

    def first_image(self):
        update_image = self.update_images.all()
        if update_image:
            try:
                return get_responsive_image(update_image[0].image)
            except IOError:
                logger.warning("Unable to fetch image for %s" % update_image)

    panels = [
        FieldPanel("description"),
        InlinePanel("update_images", heading="Images"),
        FieldPanel("post_date"),
        FieldPanel("external_link"),
    ]

    search_fields = ["description"]
