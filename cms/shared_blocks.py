from django import forms
from wagtail import blocks
from wagtail.templatetags.wagtailcore_tags import richtext
from wagtail.images.blocks import ImageChooserBlock
from wagtail.snippets.blocks import SnippetChooserBlock


class RichTextBlock(blocks.RichTextBlock):
    def get_api_representation(self, value, context=None):
        return richtext(value.source)

    class Meta:
        icon = 'doc-full'

SPACER_SIZE_SMALL = (20, "Small (20px)")

SPACER_SIZES = [
    SPACER_SIZE_SMALL,
    (40, "Medium (40px)"),
    (60, "Large (60px)"),
]

class SpacerBlock(blocks.StructBlock):
    size = blocks.ChoiceBlock(
        choices=SPACER_SIZES,
        default=SPACER_SIZE_SMALL,
        required=False
    )
    custom_height = blocks.IntegerBlock(
        required=False,
        min_value=0,
        help_text="Entering a number here will set the height in pixels and overrule the 'size' choice."
    )
    anchor = blocks.CharBlock(
        required=False,
        help_text="If you enter 'art' here, then you can add '#art' to the end of a URL to jump to this part of the page."
    )


spacer_tuple = ("spacer", SpacerBlock())


class LinkBlock(SnippetChooserBlock):
    def __init__(self, **kwargs):
        super().__init__("wagtail_links.Link", **kwargs)

    # def get_api_representation(self, value, context=None):
    #     return LinkSerializer(value).data


class CTABlock(blocks.StructBlock):
    text = blocks.CharBlock()
    link = LinkBlock()


class OptionalCTABlock(blocks.StructBlock):
    text = blocks.CharBlock(required=False)
    link = LinkBlock(required=False)

    def get_api_representation(self, value, context=None):
        if (
            value.bound_blocks.get("text").value == ""
            or value.bound_blocks.get("link").value == None
        ):
            return None
        return super().get_api_representation(value, context)


class CldImageBlock(ImageChooserBlock):
    def get_api_representation(self, value, context=None):
        # if value:
        #     return CldImageSerializer(value).data
        # else:
        return None
