from celery import shared_task
from django.core import management


@shared_task
def publish_scheduled_wagtail_pages():
    """
    Runs the wagtail mgmt command publish_scheduled_pages
    """
    management.call_command("publish_scheduled_pages")
