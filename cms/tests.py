from django.test import TestCase
from wagtail.models import Page
from .models import HomePage, ExhibitionIndexPage, ExhibitionPage


class CMSTestCase(TestCase):
    def test_exhibition(self):
        page = Page.objects.last()

        exhibition_index = ExhibitionIndexPage(title="exhibitions")
        page.add_child(instance=exhibition_index)

        exhibition_page = ExhibitionPage(title="something", start_date="2020-10-15")
        exhibition_index.add_child(instance=exhibition_page)

        url = exhibition_page.get_url()
        self.assertIn("2020", url)
        res = self.client.get(url)
        self.assertContains(res, exhibition_page.title)
