from django.urls import re_path

from .views import PressDetail


urlpatterns = [
    re_path(r'^(?P<slug>\d{4}/[-\w]*)', PressDetail.as_view(), name='press-detail'),
]
