from django.conf import settings
from django.templatetags.static import static
from django.utils.html import format_html
from django.urls import reverse
from wagtail.admin.menu import MenuItem
from wagtail import hooks
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from .models import Location, Press, Update 


@hooks.register("insert_global_admin_css", order=100)
def global_admin_css():
    """Add custom css to the admin."""
    url = "css/{}.css".format(settings.ENVIRONMENT)
    return format_html('<link rel="stylesheet" href="{}">', static(url))


@hooks.register("register_admin_menu_item")
def register_django_admin_menu_item():
    return MenuItem(
        "Django Admin",
        reverse("admin:index"),
        classnames="icon icon-folder-inverse",
        order=10000,
    )


class UpdateAdmin(ModelAdmin):
    model = Update

modeladmin_register(UpdateAdmin)


class PressAdmin(ModelAdmin):
    model = Press
    list_display = ('title', 'publisher', 'author', 'date', 'exhibition', 'artist')
    list_filter = ('publisher', 'date', 'exhibition', 'artist')
    search_fields = ('title', 'author')

modeladmin_register(PressAdmin)

class LocationAdmin(ModelAdmin):
    model = Location
    list_display = ('name', 'address')

modeladmin_register(LocationAdmin)
