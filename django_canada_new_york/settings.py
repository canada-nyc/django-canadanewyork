import os
import environ
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

env = environ.Env(DEBUG=(bool, False))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DEBUG")

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", str, ["*"])

ENVIRONMENT = env.str("ENVIRONMENT", None)
# Docker should set this
VERSION = env.str("VERSION", None)

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "django.contrib.sites",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.google",
    "allauth.socialaccount.providers.gitlab",
    "wagtail.contrib.forms",
    "wagtail.contrib.redirects",
    "wagtail.contrib.modeladmin",
    "wagtail.contrib.routable_page",
    "wagtail.contrib.settings",
    # 'wagtail.contrib.styleguide',
    "wagtail.contrib.frontend_cache",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "django_celery_results",
    "cms",  # needs to be before wagtail admin
    "wagtail.admin",
    "wagtail",
    "rest_framework",
    "modelcluster",
    "taggit",
    "corsheaders",
    "storages",
    "wagtail_links",
    "wagtail_nav_menus",
    "wagtailautocomplete",
    "debug_toolbar",
    "sass_processor",
    "compressor",
    "sekizai",
]

SITE_ID = 1

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "csp.middleware.CSPMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "wagtail.contrib.legacy.sitemiddleware.SiteMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]


def show_toolbar(request):
    return env.bool("DEBUG_TOOLBAR", False)


DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": show_toolbar}

ROOT_URLCONF = "django_canada_new_york.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates"),],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "cms.context_processors.environment",
                "sekizai.context_processors.sekizai",
            ],
        },
    },
]

WSGI_APPLICATION = "django_canada_new_york.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": env.db(default="postgres://postgres:postgres@postgres:5432/postgres"),
}


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]

SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
GOOGLE_SECRET = env.str("GOOGLE_SECRET", None)
if GOOGLE_SECRET:
    SOCIALACCOUNT_PROVIDERS = {
        "google": {
            "APP": {
                "client_id": env.str(
                    "GOOGLE_CLIENT_ID",
                    "8255101896-d3el7n2fk6ium8nqjvulk7qvpk4kdaf9.apps.googleusercontent.com",
                ),
                "secret": GOOGLE_SECRET,
                "key": "",
            }
        },
    }
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

if DEBUG is False:
    sentry_sdk.init(
        dsn=env.str(
            "SENTRY_DSN",
            "https://628851a6e17d428290096a56aff449d2@app.glitchtip.com/76",
        ),
        integrations=[DjangoIntegration()],
        environment=ENVIRONMENT,
        auto_session_tracking=False,
        traces_sample_rate=0.01,
    )

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache",}}
if os.getenv("REDIS_URL"):
    CACHES["renditions"] = env.cache("REDIS_URL")
    CELERY_BROKER_URL = env.str("REDIS_URL")

CELERY_RESULT_BACKEND = "django-db"
CELERY_TASK_SERIALIZER = "json"
BROKER_TRANSPORT_OPTIONS = {
    "fanout_prefix": True,
    "fanout_patterns": True,
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/
AWS_ACCESS_KEY_ID = env.str("AWS_ACCESS_KEY_ID", None)
AWS_SECRET_ACCESS_KEY = env.str("AWS_SECRET_ACCESS_KEY", None)
AWS_STORAGE_BUCKET_NAME = env.str("AWS_STORAGE_BUCKET_NAME", None)
AWS_S3_ENDPOINT_URL = env.str("AWS_S3_ENDPOINT_URL", None)
AWS_S3_FILE_OVERWRITE = False
AWS_LOCATION = env.str("AWS_LOCATION", "")

COLLECT_COMPILE_COMPRESS = env.bool("COLLECT_COMPILE_COMPRESS", False)
STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "sass_processor.finders.CssFinder",
    "compressor.finders.CompressorFinder",
]
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static_files")]
if COLLECT_COMPILE_COMPRESS:
    STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
    COMPRESS_STORAGE = "compressor.storage.GzipCompressorFileStorage"

COMPRESS_PRECOMPILERS = [("text/typescript", "tsc {infile} --outFile {outfile}")]
COMPRESS_OFFLINE = COLLECT_COMPILE_COMPRESS
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_URL = STATIC_URL


SASS_PROCESSOR_ROOT = STATIC_ROOT

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
if AWS_S3_ENDPOINT_URL:
    MEDIA_URL = "https://%s/%s/" % (AWS_S3_ENDPOINT_URL, AWS_LOCATION)
    DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
    AWS_QUERYSTRING_EXPIRE = 3153600000

WAGTAIL_SITE_NAME = "Canada New York"
CLOUDFLARE_API_TOKEN = env.str("CLOUDFLARE_API_TOKEN", None)
if CLOUDFLARE_API_TOKEN:
    WAGTAILFRONTENDCACHE = {
        "cloudflare": {
            "BACKEND": "wagtail.contrib.frontend_cache.backends.CloudflareBackend",
            "BEARER_TOKEN": CLOUDFLARE_API_TOKEN,
            "ZONEID": env.str("CLOUDFLARE_ZONEID", None),
        },
    }
WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.database",
        "SEARCH_CONFIG": "english",
    }
}

WAGTAIL_NAV_MENU_CHOICES_DEFAULT = [("top", "Top"), ("footer", "Footer")]

SECURE_HSTS_SECONDS = env.int("SECURE_HSTS_SECONDS", 0)

CSP_STYLE_SRC = env.list(
    "CSP_STYLE_SRC", str, ["'self'", "'unsafe-inline'", "cdn.snipcart.com",]
)
CSP_SCRIPT_SRC = env.list(
    "CSP_SCRIPT_SRC",
    str,
    [
        "'self'",
        "'unsafe-eval'",
        "'unsafe-inline'",
        "code.jquery.com",
        "cdn.snipcart.com",
        "www.googletagmanager.com",
        "https://*.google-analytics.com",
        "https://js.stripe.com",
    ],
)
CSP_DEFAULT_SRC = env.list("CSP_DEFAULT_SRC", str, ["'self'", "cdn.snipcart.com",])
CSP_CONNECT_SRC = env.list(
    "CSP_CONNECT_SRC", str, ["'self'", "*.wagtail.io", "app.snipcart.com", "https://stats.g.doubleclick.net"]
)
CSP_IMG_SRC = env.list(
    "CSP_IMG_SRC",
    str,
    [
        "'self'",
        "blob:",
        "data:",
        "https://nyc3.digitaloceanspaces.com",
        "cdn.snipcart.com",
        "https://q.stripe.com",
        "https://*.google-analytics.com",
        "https://www.gravatar.com",
    ],
)
CSP_FRAME_SRC = env.list(
    "CSP_FRAME_SRC",
    str,
    [   
        "'self'",
        "http://localhost:3000",
        "https://www.canadanewyork.com/",
        "https://www.youtube-nocookie.com",
        "www.google.com",
        "www.youtube.com",
        "https://open.spotify.com",
        "player.vimeo.com",
        "https://gfycat.com",
        "https://js.stripe.com",
    ],
)
CSP_REPORT_URI = env.list("CSP_REPORT_URI", str, ["https://app.glitchtip.com/76/security/?sentry_key=628851a6e17d428290096a56aff449d2"])

CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS", str, [])

CORS_ORIGIN_ALLOW_ALL = env.str("CORS_ORIGIN_ALLOW_ALL", True)
CORS_ORIGIN_WHITELIST = env.list("CORS_ORIGIN_WHITELIST", str, [])

ENVIRONMENT = env.str("ENVIRONMENT", "local")

GOOGLE_TAG_MANAGER_ID = env.str("CANADA_GOOGLE_TAG_MANAGER_ID", "")

SNIPCART_API_KEY = env.str("CANADA_SNIPCART_API_KEY", "")

DATA_UPLOAD_MAX_NUMBER_FIELDS = 2000

SECTION_PIXEL_WIDTH = 800
SECTION_PIXEL_WIDTH_STR = str(SECTION_PIXEL_WIDTH)

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
