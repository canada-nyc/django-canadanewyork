from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from django.views.generic import TemplateView
from wagtail.admin import urls as wagtailadmin_urls
from wagtail import urls as wagtail_urls
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.documents import urls as wagtaildocs_urls
from wagtailautocomplete.urls.admin import urlpatterns as autocomplete_admin_urls
import debug_toolbar

from cms import urls as cms_urls

from .views import health


def redirect_to_cms(request):
    return redirect("/cms/")


urlpatterns = [
    path("_health/", health),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    path("cms/autocomplete/", include(autocomplete_admin_urls)),
    path("admin/", admin.site.urls),
    path("cms/", include(wagtailadmin_urls)),
    path("sitemap.xml", sitemap),
    path("documents/", include(wagtaildocs_urls)),
    path("accounts/", include("allauth.urls")),
    path("press/", include(cms_urls)),
    path("__debug__/", include(debug_toolbar.urls)),
    path("", include(wagtail_urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
