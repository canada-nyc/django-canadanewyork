def sentence_join(list):
    '''
    Returns the list joined properly to add to text.

    For example:

    first, second, and third
    first and second
    first
    '''
    if not len(list):
        return ''
    if len(list) == 1:
        return list[0]
    elif len(list) == 2:
        return ' and '.join(list)
    else:
        list[-1] = 'and ' + list[-1]
        return ', '.join(list)
